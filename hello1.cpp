/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07c - My First Cat - EE 205 - Spr 2022
///
/// @file hello1.cpp
/// @version 1.0
///
/// @author Liam Tapper <tliam@hawaii.edu>
/// @date 25_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#include <iostream> //needed for end1
using namespace std; //no longer needs std
int main() {
  cout << "Hello, World!" << endl;
  return 0;
}
