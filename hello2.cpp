/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07c - My First Cat - EE 205 - Spr 2022
///
/// @file hello2.cpp
/// @version 1.0
///
/// @author Liam Tapper <tliam@hawaii.edu>
/// @date 25_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#include<iostream> //for std and end1
int main() {

   std::cout << "Hello, World!" << std::endl;
   return 0;
}
